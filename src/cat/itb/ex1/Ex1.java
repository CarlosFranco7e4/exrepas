package cat.itb.ex1;

public class Ex1 {

    public static void main(String[] args) {
        Thread t1 = new Thread(new PrintThread("Una vegada hi havia un gat"));
        Thread t2 = new Thread(new PrintThread("Once upon a time in the west"));
        Thread t3 = new Thread(new PrintThread("En un lugar de la Mancha"));
        t1.start();
        t2.start();
        t3.start();
    }

    private static class PrintThread implements Runnable {
        private String frase;

        public PrintThread(String s) {
            this.frase = s;
        }

        private static synchronized void mostrar (String frase) {
            String[] frasePerParts = frase.split(" ");
            try {
                for (int i = 0; i < frasePerParts.length; i++) {
                    System.out.print(frasePerParts[i]+" ");
                    Thread.sleep(1000);
                }
                System.out.println();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            mostrar(frase);
        }
    }
}
